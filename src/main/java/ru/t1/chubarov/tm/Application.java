package ru.t1.chubarov.tm;

import static ru.t1.chubarov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
        }
    }

    private static void showError() {
        System.out.println("[ERROR]");
        System.out.println("This argument not supported");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeni Chubarov");
        System.out.println("e-mail: echubarov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show about program. \n", ABOUT);
        System.out.printf("%s - Show program version \n", VERSION);
        System.out.printf("%s - Show list arguments \n", HELP);
    }

}
